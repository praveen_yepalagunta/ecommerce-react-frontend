import axios from "axios";
import React, { useState } from "react";
import { Link} from "react-router-dom";
// import Products from "./products";



const ProdEdit=()=>{
    
    const[id,idchange]=useState("");
    const[prodname,prodnamechange]=useState("");
    const[price,pricechange]=useState("");
    const[Quantity,quantitychange]=useState("");
    const[Description,descriptionchange]=useState("");
    const[category,categorychange]=useState("")
        const handlesubmit=(e)=>{
            e.preventDefault();
            
            console.log({id,prodname,price,Quantity,Description});
            
        axios.put(`http://localhost:8080/ServerletJspDemo/ProductsApi?prod_id=${id}`,null,
        {
             params:{
                id,
                prodname,
                price,
                Quantity,
                Description,
                category,
            }})
    
        }

    return(
        <div>
            <div className="row">
                <div className="offset-lg-3 col-lg-6">
                    <form className="container1" onSubmit={handlesubmit}>
                        <div className="card" style={{"textAlign":"left"}}>
                            <div className="card-title">
                                <h2>Product Editing</h2>
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>id</label>
                                            <input type="number" required value={id} onChange={e=>idchange(e.target.value)}  className="form-control"/>
                                        </div>
                                    </div> 
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Name</label>
                                            {/* <input type="number" required value={id} onChange={e=>idchange(e.target.value)}  className="form-control" hidden></input> */}
                                            <input value={prodname} required onChange={e=>prodnamechange(e.target.value)} className="form-control"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Price</label>
                                            <input type="number" value={price || ""} required onChange={e=>pricechange(e.target.value)}  className="form-control"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Quantity</label>
                                            <input type="number" value={Quantity || ""} required onChange={e=>quantitychange(e.target.value)}  className="form-control"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Description</label>
                                            <input value={Description || ""} required onChange={e=>descriptionchange(e.target.value)}  className="form-control" type="text"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Category</label>
                                            <input value={category || ""} required onChange={e=>categorychange(e.target.value)}  className="form-control" type="text"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <button className="btn btn-success" type="submit">Save</button>
                                            <Link to="/product" className="btn btn-danger">Back</Link>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>

            </div>

        </div>
    );
    }
export default ProdEdit;