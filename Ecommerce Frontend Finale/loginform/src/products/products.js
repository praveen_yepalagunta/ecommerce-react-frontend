import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
// import { newContext } from "./App";
import ProdEdit from "./ProdEdit";
const Products=()=>{
    const[productdata,productchange]=useState(null);
    const navigate=useNavigate();
    const Loadfunction=(id)=> {
        navigate('/product/Edit')


    }
    const Removefunction=(prod_id)=>{
        if(window.confirm('Do you want to Delete?'))
        axios.delete(`http://localhost:8080/ServerletJspDemo/ProductsApi?prod_id=${prod_id}`)
         .then((response) => {
             console.log(response);
             })
//        
console.log(prod_id);
}

useEffect(()=>{
    fetch("http://localhost:8080/ServerletJspDemo/AllProducts")
    .then((res)=>{
      return res.json();
    }).then((resp)=>{
        productchange(resp);
     })
    .catch((err)=>{
        console.log(err.message);
    })
},[])
    return(
        <div className="container2">
            <div className="card">
                
                <div className="card-body">
                <div className="card-title">
                    <h2>Product Details</h2>
                    <div className="card-body">
                </div>
                </div>
                    <div className="divbtn">
                        <Link to="product/create" className="btn btn-success">ADD New (+)</Link>
                    </div>
                    {/* <div className="text-right">
                         <Link to="/" className="btn text-right btn-success">MyCart</Link>
                    </div> */}
                    <table className="table table-bordered">
                        <thead className="bg-dark text-white">

                            <tr>
                                <td>Id</td>
                                <td>Name</td>
                                <td>price</td>
                                <td>Quality</td>
                                <td>Description</td>
                                <td>Images</td>
                                <td>Options</td>
                                
                            </tr>

                        </thead>
                        <tbody>
                            {   productdata &&
                                productdata.map(items=>(
                                    <tr key={items.id}>
                                        <td>{items.id}</td>
                                        <td>{items.productName}</td>
                                        <td>{items.price}</td>
                                        <td>{items.qty}</td>
                                        <td>{items.prodDesc}</td>
                                        <td><img src={items.image}/></td>
                                        
                                       
                                        <td><a onClick={()=>{Loadfunction(items.id)}} className="btn btn-success">Edit</a>
                                        <a onClick={()=>{Removefunction(items.id)}} className="btn btn-danger">Remove</a>
                                        
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    )
}
export default Products;