import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import ProdCreate from './productCreate';
import ProdEdit from './ProdEdit';
import Products from './products';
import React, { useState, createContext } from 'react';
import DropDownList from '../DropDownList';
import { isEditable } from '@testing-library/user-event/dist/utils';

export const newContext = createContext();
// export const newdata={id,prodname,price,qty,prodDesc};

function App() {
  const [isEditable,setIsEditable] = useState(true);
  return (
    <div className="App">
      <BrowserRouter>
        <newContext.Provider value={{isEditable,setIsEditable}}>
          <Routes>
            <Route path='/' element={<Products></Products>}></Route>
            
            <Route path='/product/create' element={<ProdCreate></ProdCreate>}></Route>
          <Route path='/product/Edit' element={<ProdEdit ></ProdEdit>}></Route>
          </Routes>
        </newContext.Provider>
      </BrowserRouter>
    </div>
  );

}

export default App;
