import { Link} from 'react-router-dom';
import { useState } from 'react';
import axios from 'axios';
const ProdCreate = () => {
    const[id,idchange]=useState("");
    const[prodname,prodnamechange]=useState("");
    const[price,pricechange]=useState("");
    const[Quantity,quantitychange]=useState("");
    const[Description,descriptionchange]=useState("");
    const[Category,categorychange]=useState("");
    // const[productdata,productchange]=useState(null);
    
        


    // }
    const handlesubmit=(e)=>{
        e.preventDefault();
        // const Proddata={id,prodname,price,Quantity,Description};
        // console.log({id,prodname,price,Quantity,Description});
        
        

        // fetch("http://localhost:8080/ServerletJspDemo/ProductsApi",{
        //     method:"POST",
        //     headers:{"content-type":"application/json"},
        //     body:JSON.stringify(Proddata)
        // }).then((res)=>{
        //   alert("saved Successfully");
        //   navigate('/');
        // }).catch((err)=>{
        //     console.log(err.message)

        // })
        axios.post(`http://localhost:8080/ServerletJspDemo/ProductsApi`, null, { params: {
            id,
            prodname,
            price,
            Quantity,
            Description,
            Category
          }})
          .then(response => response.status)
          .catch(err => console.warn(err));

    }
    return (
        <div>
            <div className="row">
                <div className="offset-lg-3 col-lg-6">
                    <form className="container1" onSubmit={handlesubmit}>
                        <div className="card" >
                        {/* style={{"textAlign":"left"}} */}
                            <div className="card-title">
                                <h2>Product Create</h2>
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Id</label>
                                            <input type="number" required value={id} onChange={e=>idchange(e.target.value)}  className="form-control"></input>
                                        </div>
                                    </div> 
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Name</label>
                                            <input value={prodname || ""} required onChange={e=>prodnamechange(e.target.value)} className="form-control"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Price</label>
                                            <input type="number" value={price || ""} required onChange={e=>pricechange(e.target.value)}  className="form-control"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Quantity</label>
                                            <input type="number" value={Quantity || ""} required onChange={e=>quantitychange(e.target.value)}  className="form-control"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Description</label>
                                            <input value={Description || ""} required onChange={e=>descriptionchange(e.target.value)}  className="form-control" type="text"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <label>Product Category</label>
                                            <input value={Category || ""} required onChange={e=>categorychange(e.target.value)}  className="form-control" type="text"></input>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <button className="btn btn-success" type="submit">save</button>
                                            <Link to="/product" className="btn btn-danger">Back</Link>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>

            </div>

        </div>
    );
}
export default ProdCreate;