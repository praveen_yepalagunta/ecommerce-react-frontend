import React, { useState, useEffect } from "react";
import Selectdd from "./DropDownList";
// import "./index.css";
import axios from "axios";
import { Link } from "react-router-dom";
// import Header from "./Header";

function Dropdown() {

    const [inputData,setInputData] = useState([]);
    const [selectoption, setSelectoption] = useState("Select Brand");

  

  // useEffect(() => {
  //   axios.get(`http://localhost:8080/ServerletJspDemo/AllProducts`)
  //     .then(res => setInputData(res.data))
  //     .catch(err => console.log(err))
  //     console.log(inputData)

  // }, [])

  useEffect(()=>{
    fetch("http://localhost:8080/ServerletJspDemo/AllProducts")
    .then((res)=>{
      return res.json();
    }).then((resp)=>{
        setInputData(resp);
        console.log(inputData);

    })
    .catch((err)=>{
        console.log(err.message);
    })
},[])

  

  const renderAllData = () => {
    return (
        <div id="d111">
        {/* <Header></Header> */}
        <div className="container" id="d222">
          <div className="row">
            <div className="col-md-8 offset-md-2 border rounded p-4 mt-2 shadow" id="d333">
              <h2 className="text-center m-4" id="h2">Product Details</h2>
              <div className="card">
                {inputData.map((u) =>
                  <div className="card-header" id="d444">
                    <b>CATEGORY : {u.category}</b>
                    <ul className="list-group list-group-flush d-flex">
                      {/* <li className="list-group-item">
                    <b>Id:</b>
                    {u.prd_id}
                  </li> */}
                      <li className="list-group-item">
                       
                        <b>Name:{u.productName}</b>
                        {/* {u.prod_name} */}
                      </li>
                      <li className="list-group-item">  
                        <b>Description:</b>
                        {u.prodDesc}
                      </li>
  
                      <li className="list-group-item">
                        <b>Price:</b>
                        {u.price}
                      </li>
                      <li className="list-group-item">
                        <b>Quantity</b>
                        {u.qty}
                      </li>
                      <li className="list-group-item">
                        <b></b>
                      </li>
                      {/* <li className="list-group-item">
                        <Link to="/Edit"><button type="submit" className="btn btn-sml btn-success" onClick={() => setEditingDetails(u)} id="fb2">Edit</button></Link>
                        <button type="submit" className="btn btn-sml btn-danger-space" onClick={deletePost} id="fb1">Delete</button>
                      </li> */}
                    </ul>
                  </div>)}
              </div>
  
            </div>
          </div>
        </div>
  
  
  
  
      </div>
    );
  };

  const renderBrandData = () => {
    console.log(selectoption)
    const filteredItems = inputData.filter((obj) => obj.category === selectoption);

    return (
    //   <ul className="ulList">
    //     {filteredItems.map((obj) => (
    //       <li className="liList" key={obj.prd_id}>
    //         <div className="itemsDiv">
    //         <div className="div1">{obj.prd_name}</div>
    //           <div className="div2">{obj.prd_price}</div>
    //           <div className="div3">{obj.prd_desc}</div>
    //           <div className="div4">{obj.prd_cat}</div>
    //         </div>
    //       </li>
    //     ))}

<div id="d111">
      {/* <Header></Header> */}
      <div className="container" id="d222">
        <div className="row">
          <div className="col-md-8 offset-md-2 border rounded p-4 mt-2 shadow" id="d333">
            <h2 className="text-center m-4" id="h2">Product Details</h2>
            <div className="card">
              {filteredItems.map((u) =>
                <div className="card-header" id="d444">
                  <b>CATEGORY : {u.category}</b>
                  <ul className="list-group list-group-flush d-flex p-10">
                    {/* <li className="list-group-item">
                  <b>Id:</b>
                  {u.prd_id}
                </li> */}
                    <li className="list-group-item">
                      <b>Name:</b>
                      {u.productName}
                    </li>
                    <li className="list-group-item">  
                      <b>Description:</b>
                      {u.prodDesc}
                    </li>

                    <li className="list-group-item">
                      <b>Price:</b>
                      {u.price}
                    </li>
                    <li className="list-group-item">
                        <b>ID:</b>
                        {u.id}
                      </li>
                    <li className="list-group-item">
                      <b></b>
                    </li>
                    {/* <li className="list-group-item">
                      <Link to="/Edit"><button type="submit" className="btn btn-sml btn-success" onClick={() => setEditingDetails(u)} id="fb2">Edit</button></Link>
                      <button type="submit" className="btn btn-sml btn-danger-space" onClick={deletePost} id="fb1">Delete</button>
                    </li> */}
                  </ul>
                </div>)}
            </div>

          </div>
        </div>
      </div>




    </div>


    //   </ul>
    );
  };

  return (
    <div>
        {/* <Header></Header> */}
      <Selectdd
        setSelectoption={setSelectoption}
        selectoption={selectoption}
      ></Selectdd>

      {selectoption === "Select Brand" ? renderAllData() : renderBrandData()}
      <Link to="/product"><button>Go to the products list</button></Link>

{/* <div className="card">
              {inputData.map((u) =>
                <div className="card-header" id="d444">
                  <b>Details of Product id : {u.prd_id}</b>
                  <ul className="list-group list-group-flush d-flex p-10"> */}
                    {/* <li className="list-group-item">
                  <b>Id:</b>
                  {u.prd_id}
                </li> */}
                    {/* <li className="list-group-item">
                      <b>Name:</b>
                      {u.prd_name}
                    </li>
                    <li className="list-group-item">  
                      <b>Description:</b>
                      {u.prd_desc}
                    </li>

                    <li className="list-group-item">
                      <b>Price:</b>
                      {u.prd_price}
                    </li>
                    <li className="list-group-item">
                      <b>Category:</b>
                      {u.prd_cat}
                    </li>
                    <li className="list-group-item">
                      <b></b>
                    </li> */}
                    {/* <li className="list-group-item">
                      <Link to="/Edit"><button type="submit" className="btn btn-sml btn-success" onClick={() => setEditingDetails(u)} id="fb2">Edit</button></Link>
                      <button type="submit" className="btn btn-sml btn-danger-space" onClick={deletePost} id="fb1">Delete</button>
                    </li> */}
                  {/* </ul>
                </div>)}
            </div> */}

    </div>
  );
}

export default Dropdown;
// {`${obj.category} ${obj.brand} ${obj.description} ${obj.price}`}
// {`${obj.category} ${obj.description} ${obj.price}`}
