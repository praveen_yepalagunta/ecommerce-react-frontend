import './App.css';
import './products/App.css'
import React,{useState} from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Login } from './login';
import { Register } from './Register';
import ProdEdit from './products/ProdEdit';
import ProdCreate from './products/productCreate';
import Products from './products/products';
import Dropdown from './DropTwo';


function App() {
  const[currentForm,setCurrentForm]=useState('login');
  const toggleForm=(formName)=>{
    setCurrentForm(formName);
  }
  return (
    <div className="App">
      <BrowserRouter> 
        
          
            {/* {currentForm=== "login"?<Login onFormSwitch={toggleForm}/>:<Register onFormSwitch={toggleForm}/>} */}
            <Routes>
              <Route path="/" element={currentForm=== "login"?<Login onFormSwitch={toggleForm}/>:<Register onFormSwitch={toggleForm}/>}></Route>
              <Route path='/login' element={<Login></Login>}></Route>
              <Route path='/Dropdown' element={<Dropdown></Dropdown>}></Route>
              <Route path='/' element={<Register></Register>}></Route>
              {/* {/* <Route path='/' element={<Login></Login>}></Route> */}
              <Route path='/registration' element={<Register></Register>}></Route>
            <Route path='/product' element={<Products></Products>}></Route>
            <Route path='/product/product/create' element={<ProdCreate></ProdCreate>}></Route>
          <Route path='/product/Edit' element={<ProdEdit ></ProdEdit>}></Route>
          </Routes>
        
      </BrowserRouter>

      {/* <Dropdown></Dropdown> */}
    {/* </div>
    // <div className="App">
    //   <BrowserRouter>
    //   <Route>{currentForm=== "login"?<Login onFormSwitch={toggleForm}/>:<Register onFormSwitch={toggleForm}/>
    //  }
    //   <Route/> */}
    
    {/* <BrowserRouter/> */}
    </div>
  );
}

export default App;
