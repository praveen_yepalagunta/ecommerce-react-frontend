// import axios from "axios";
import React,{ useState} from "react";
import { Link, useNavigate } from "react-router-dom";
import './App.css'


export const Login=(props)=>{
    const[username,setUsername]=useState("");
    const[password,setpassword]=useState("");
    const Navigate = useNavigate()
    
    
    const handleSubmit=async (e)=>{
        e.preventDefault();
        // console.log({username},{password});
       const response=await fetch(`http://localhost:8080/ServerletJspDemo/Api?username=${username}&password=${password}`,{method: "GET",})
       const data = await response.json()
       console.log(data)
       if((username===data.username) && (password===data.password)){
        alert("Login Successfully")
        Navigate('/Dropdown');
    }else{
        alert("please fill the details correctly");
    }

    }
    return(
           
        <div className="container">
             <h2>Login Form</h2>
        <form className="login-form" onSubmit={handleSubmit}>
            <label for="username">username</label>
            <input required value={username} onChange={(e)=>setUsername(e.currentTarget.value)} type="text" placeholder="enter your username" id="username"/>
            
            <label for="password">Password</label>
            <input required value={password} onChange={(e)=>setpassword(e.target.value)} type="password" placeholder="*********" id="password"/>
            <button className="login" type="submit" >Log In</button>
            
        </form>
      <Link to="/registration"  ><button className="link-btn">Dont have an account? Register</button></Link>

      
        {/* <button className="link-btn" onClick={()=>props.onFormSwitch('register')}>Dont have an account? Register</button> */}
        </div>
        
    )
}
    
