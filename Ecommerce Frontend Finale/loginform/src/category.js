import React, { useState, useEffect } from "react"; 
const DropdownList = () => {
    const [options, setOptions] = useState([]);
    const [selectedOption, setSelectedOption] = useState(""); useEffect(() => {
        // Fetch data from API and update the state
        fetch(`http://localhost:8080/ServerletJspDemo/ProductsApi?category=${options.category}`)
            .then((response) => response.json())
            .then((data) => setOptions(data))
            .catch((error) => console.log(error));
    }, []); const handleOptionChange = (event) => {
        setSelectedOption(event.target.value);
    };
    return (
        <div>
            <select value={selectedOption} onChange={handleOptionChange}>
                {options.map((option) => (<option key={option.category} value={option.category}>
                    {option.category}</option>
                ))}</select></div>
    );
};
export default DropdownList;