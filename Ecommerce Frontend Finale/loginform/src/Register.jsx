import axios from "axios";
import React,{useState} from "react";
import { Link } from "react-router-dom";
import './App.css';
export const Register=(props)=>{
    const[email,setEmail]=useState('');
    const[password,setpassword]=useState('');
    const[username,setname]=useState('');
    
    const handleSubmit=(e)=>{
        e.preventDefault();
        console.log({email},{password},{username});
        axios.post(`http://localhost:8080/ServerletJspDemo/Api`, null, { params: {
            username,
            password,
            email
          }})
          alert("Register Successfully")
          .then(response => response.status)
          .catch(err => console.warn(err));

    }
    return(
        <div className="container">
            <div className="text-right d-flex flex-row pr-15">
                         <Link to="/" className="btn text-right btn-danger">LogOut</Link>
                         <h2>Registration Form</h2>
                    </div>
            
        <form className="register-form" onSubmit={handleSubmit}>
            <label htmlFor="name">Full Name</label>
            <input value={username} onChange={(e)=>setname(e.currentTarget.value)} name="name" type="text" placeholder="full name"/>
            <label htmlFor="email">Email</label>
            <input value={email} onChange={(e)=>setEmail(e.currentTarget.value)} type="email" placeholder="youremail@gmail.com" id="email"/>
            <label htmlFor="password">Password</label>
            <input value={password} onChange={(e)=>setpassword(e.target.value)} type="password" placeholder="*********" id="password"/>
            <button className="login" type="submit">Login</button>
        </form>
     <Link to='/'>   <button className="link-btn">Alreay have an account? Register</button></Link>
        </div>
    )
}